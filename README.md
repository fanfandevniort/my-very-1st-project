[ENGLISH]
As my very first project, on GitLab, I decided to bring my resume to the entire world. ;)
This resume, made with HTML, CSS and JavaScript jQuery is going to help me to find a company for an intership during a couple of months.

[FRANÇAIS]
Comme tout premier projet sur GitLab, j'ai choisi de mettre mon CV à disposition du monde entier. ;)
Ce CV, réalisé en HTML, CSS and JavaScript jQuery, va m'aider à trouver une entreprise pour un stage d'une durée de deux mois.